package com.davidgod93.tinder;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class Main extends AppCompatActivity {

	int sexo = -1;
	Biblioteca b = new Biblioteca();
	ImageView v;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
		cambiaModo();
    }

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main_menu, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()) {
			case R.id.settings:
				Toast.makeText(this, "Apretado Settings", Toast.LENGTH_SHORT).show();
				break;
			case R.id.favorito:
				Toast.makeText(this, "Función en la próxima versión", Toast.LENGTH_SHORT).show();
				break;
			case R.id.modo:
				cambiaModo();
				break;
		}
		return super.onOptionsItemSelected(item);
	}

	private void cambiaModo() {
		String[] o = new String[]{"Chicos", "Chicas", "Ambos"};
		new AlertDialog.Builder(this)
				.setTitle("Selecciona que quieres valorar")
				.setCancelable(false)
				.setItems(o, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						sexo = which;
						log("Hemos seleccionado "+which);
						init();
					}
				})
				.show();
	}

	private void init() {
        setContentView(R.layout.activity_main);
		v = (ImageView) findViewById(R.id.imageView);
		getNext();
	}

	private void log(String l) {
		Log.println(Log.ASSERT, "TINDER", l);
	}

	public void cambiafotoS(View v) {
		log("Apretado Sí");
		getNext();
	}

	public void cambiafotoN(View v) {
		log("Apretado No");
		getNext();
	}

	private void getNext() {
		List<Chic> x = b.getChicos();
		List<Chic> y = b.getChicas();
		boolean esChico = sexo == 0 || (sexo != 1 && randomSex());
		final Chic c = esChico ? x.get(rand(x.size())) : y.get(rand(y.size()));
		final String f = getRandomFile(c.esChico);
		cambiaFoto(f);
		v.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(Main.this, PerfilActivity.class);
				i.putExtra("nombre", c.nombre);
				i.putExtra("localidad", c.localidad);
				i.putExtra("trabajo", c.trabajo);
				i.putExtra("img", f);
				startActivity(i);
			}
		});
	}

	private boolean randomSex() {
		return Math.random() > 0.5;
	}

	private void cambiaFoto(String uri) {
		int u = getResources().getIdentifier(uri, "drawable", getPackageName());
		Picasso.with(this).load(u).into(v);
	}

	private int rand(int max) {
		return (int)(Math.random()*max);
	}

	private String getRandomFile(boolean esChico) {
		Field[] sel = R.drawable.class.getFields();
		List<String> ss = new ArrayList<>();
		String pref = esChico ? "y_" : "x_";
		for(Field aSel : sel) if(aSel.getName().startsWith(pref)) ss.add(aSel.getName());
		return ss.get(rand(ss.size()));
	}
}
