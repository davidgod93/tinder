package com.davidgod93.tinder;

/**
 * Created by David on 22/04/2016.
 */
public class Chic {
	public boolean esChico, esFavorito;
	public String nombre, trabajo, localidad;

	public Chic(String nombre, String trabajo, String localidad) {
		this(nombre, trabajo, localidad, true, false);
	}

	public Chic(String nombre, String trabajo, String localidad, boolean esChico) {
		this(nombre, trabajo, localidad, esChico, false);
	}

	public Chic(String nombre, String trabajo, String localidad, boolean esChico, boolean favorito) {
		this.nombre = nombre;
		this.trabajo = trabajo;
		this.localidad = localidad;
		this.esChico = esChico;
		this.esFavorito = favorito;
	}

	public String toString() { return nombre; }
}
