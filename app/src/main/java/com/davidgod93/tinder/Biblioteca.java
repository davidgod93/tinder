package com.davidgod93.tinder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by David on 22/04/2016.
 */
public class Biblioteca {
	private List<Chic> chicos;
	private List<Chic> chicas;

	public Biblioteca() {
		chicos = new ArrayList<>();
		chicas = new ArrayList<>();
		rellena();
	}

	public void rellena() {
		chicos.add(new Chic("Alberto Pons Gualda", "Mecánico", "Valencia"));
		chicos.add(new Chic("Juan Piñeiro Giménez", "Médico", "Paterna"));
		chicos.add(new Chic("Pedro Peña Guijarro", "Sacerdote", "Alaqüas"));
		chicos.add(new Chic("Esteban Prieto Piris", "Profesor", "Paiporta"));
		chicos.add(new Chic("Víctor Rubio García", "Chófer", "Alfafar"));
		chicos.add(new Chic("Vicente Ramón González", "Banquero", "Catarroja"));

		chicas.add(new Chic("Jennifer Pérez Ruiz", "Abogada", "Torrent", false));
		chicas.add(new Chic("Carla Plà Cerdà", "Informática", "Sedaví", false));
		chicas.add(new Chic("Ana Pons Gualda", "Forense", "Alboraya", false));
		chicas.add(new Chic("Sofia Piña Gualda", "Criminóloga", "Benimamet", false));
		chicas.add(new Chic("Gloria Pons Gualda", "Cajera", "Burjassot", false));
		chicas.add(new Chic("Celia Pons Gualda", "Peluquera", "Alzira", false));
	}

	public List<Chic> getChicos() {
		return chicos;
	}

	public List<Chic> getChicas() {
		return chicas;
	}

	public List<Chic> getFavoritos() {
		List<Chic> c = new ArrayList<>();
		c.addAll(chicos);
		c.addAll(chicas);
		return c;
	}
}
