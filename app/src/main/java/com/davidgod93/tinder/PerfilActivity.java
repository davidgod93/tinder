package com.davidgod93.tinder;

import android.app.ActionBar;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class PerfilActivity extends AppCompatActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ActionBar ab;
		if((ab = getActionBar()) != null) {
			ab.setHomeButtonEnabled(true);
			ab.setHomeAsUpIndicator(R.mipmap.icon);
		}
		Intent i = getIntent();
		Bundle extras = i.getExtras();
		String nombre = extras.getString("nombre");
		String localidad = extras.getString("localidad");
		String trabajo = extras.getString("trabajo");
		String foto = extras.getString("img");

		setContentView(R.layout.activity_perfil);
		TextView nombreText = (TextView) findViewById(R.id.textView4);
		TextView localidadText = (TextView) findViewById(R.id.textView5);
		TextView trabajoText = (TextView) findViewById(R.id.textView6);
		ImageView img = (ImageView) findViewById(R.id.imageView);

		nombreText.setText(nombre);
		localidadText.setText(localidad);
		trabajoText.setText(trabajo);

		Picasso.with(this).load(getResources().getIdentifier(foto, "drawable", getPackageName())).into(img);
	}
}
